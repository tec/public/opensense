# Welcome to OpenSense at ETH Zürich

We are part of the [OpenSense](http://opensense.epfl.ch/wiki/index.php/OpenSense_2.html) research project which is funded by [Nano-Tera](http://www.nano-tera.ch/).
OpenSense aims at investigating community-based sensing using wireless sensor network technology to monitor air pollution.

## About the project

Please have a look at our [OpenSense wiki](https://gitlab.ethz.ch/tec/public/opensense/wikis/home) to find more information about the project and our deployment.

## Public Air Pollution Dataset

### Ultrafine Particle Dataset
Two and a half years (04/2012-12/2014, >36 Mio samples) of UFP data collected by the mobile sensor network can be found here: [https://zenodo.org/record/3298842](https://zenodo.org/record/3298842)

### Ozone and Carbon Monoxide Dataset
Over four years of ozone (O3) and two years of carbon monoxide (CO) measurements (~70 Mio. samples) by the mobile sensor network can be found here: [https://doi.org/10.5281/zenodo.3355208](https://doi.org/10.5281/zenodo.3355208)

### Test datasets
Two test datasets can be found here:
* [Ozone (O3)](data/ozon_tram1_14102011_14012012.csv)
* [Ultrafine particulate matter (UFP)](data/pm_tram1_14102011_14012012.csv)

### Data request
If you would like to use more of our data for your project please contact us at osense(at)tik.ee.ethz.ch
